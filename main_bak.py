import RPi.GPIO as GPIO
import time
import datetime

from samplebase import SampleBase
from rgbmatrix import graphics

class RunText(SampleBase):
	GPIO.setmode(GPIO.BCM)

	GPIO.setup(18, GPIO.IN, pull_up_down=GPIO.PUD_UP)

	timeSinceChurn = 0
	checkPerSecond = 10

	sleepAmount = 0.1 #1/checkPerSecond
	sleepCounter = 0;
	screenWidth = 64
	screenHeight = 32
	textColor = graphics.Color(255, 0, 0)
	
	def __init__(self, *args, **kwargs):
		super(RunText, self).__init__(*args, **kwargs)
		self.parser.add_argument("-t", "--text", help="The text to scroll on the RGB LED panel", default="Hello world!")
	
	def getPositionForCenterText(self, string, screenWidth, fontWidth):
		stringLength = len(string)
		centerOfScreen = screenWidth/2
		textWidth = fontWidth * stringLength
		return centerOfScreen - textWidth/2
	
	def run(self):
	
	
		offscreen_canvas = self.matrix.CreateFrameCanvas()
		font = graphics.Font()
		font.LoadFont("fonts/7x13.bdf")
		
		sysOutFont = graphics.Font()
		sysOutFont.LoadFont("fonts/4x6.bdf")
		
		textColor = graphics.Color(255, 0, 0)
		pos = 5
		my_text = self.args.text

		
		while True:
		
			offscreen_canvas.Clear()
			if self.sleepCounter >= self.checkPerSecond:
				self.timeSinceChurn += 1
				self.sleepCounter = 0
				print str(datetime.timedelta(seconds=self.timeSinceChurn))

				
				
			self.displayCenteredText('Time Since Churn', sysOutFont, 4, 6, 0, graphics, offscreen_canvas)
			
			input_state = GPIO.input(18)
			if input_state == False:
				self.timeSinceChurn = 0
				timeResetText = 'Time Reset!'
				print(timeResetText)
				self.displayCenteredText(timeResetText, sysOutFont, 4, 6, 20, graphics, offscreen_canvas)

			timeElapsed = str(datetime.timedelta(seconds=self.timeSinceChurn))
			self.displayCenteredText(timeElapsed, font, 7, 13, 5, graphics, offscreen_canvas)

			time.sleep(0.05)
			offscreen_canvas = self.matrix.SwapOnVSync(offscreen_canvas)

			time.sleep(self.sleepAmount)
			self.sleepCounter += 1

	def displayCenteredText(self, textToDisplay, fontToUse, fontSizeWidth, fontSizeHeight, topOfText, graphicsObject, canvasObject):
		graphicsObject.DrawText(canvasObject, fontToUse, self.getPositionForCenterText(textToDisplay, self.screenWidth, fontSizeWidth), topOfText + fontSizeHeight, self.textColor, textToDisplay)
			
			# Main function
if __name__ == "__main__":
    run_text = RunText()
    if (not run_text.process()):
        run_text.print_help()
