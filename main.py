import time, datetime

from rgbmatrix 		import graphics
from DisplayFont 	import DisplayFont
from DisplayDriver 	import DisplayDriver
from CheckButton 	import CheckButton
from DisplayText	import DisplayText

class ChurnBoard():

	# Setup the CheckButton to watch for GPIO 18
	buttonOn18 = CheckButton(18)
	
	# Has to be after GPIO setup, not sure why.
	displayDriver = DisplayDriver()
		
	timeSinceChurn = 0
	checkPerSecond = 10

	sleepAmount = 0.1 #1/checkPerSecond
	sleepCounter = 0;
	
	def run(self):
		
		sysOutFont 	= DisplayFont("fonts/4x6.bdf", 4, 6, graphics.Color(255, 0, 0))
		mainFont 	= DisplayFont("fonts/7x13.bdf", 7, 13, graphics.Color(255, 0, 0))
		
				
		while True:
			
			if self.sleepCounter >= self.checkPerSecond:
				self.timeSinceChurn += 1
				self.sleepCounter = 0
			self.displayDriver.addDisplayItem( DisplayText('Time Since Churn', sysOutFont, (0,0), centered = True) )
						
			if self.buttonOn18.isButtonPushed() == True:
				self.timeSinceChurn = 0
				self.displayDriver.addDisplayItem( DisplayText('Time Reset!', sysOutFont, (0,0), centered = True, displayFromBottom = True) )

			timeElapsed = str(datetime.timedelta(seconds=self.timeSinceChurn))
			self.displayDriver.addDisplayItem( DisplayText(timeElapsed, mainFont, (0,5), centered = True) )

			self.displayDriver.flush()
			
			time.sleep(self.sleepAmount)
			self.sleepCounter += 1

			
			# Main function
if __name__ == "__main__":
    churnBoard = ChurnBoard()
    churnBoard.run()
