class DisplayText():
	
	location 			= (0,0)
	font 				= None
	text				= None
	centered			= False
	displayFromBottom 	= False
	
	def __init__(self, text, font, location, **keywords):
		self.text 		= text
		self.font 		= font
		self.location 	= location
		
		if 'centered' in keywords:
			self.centered 	= keywords['centered']
			
		if 'displayFromBottom' in keywords:
			self.displayFromBottom 	= keywords['displayFromBottom']
	
	def display(self, graphicsObject, canvasObject):
		x = self._getPositionForCenterText(canvasObject) if self.centered else self.location[0]
		y = self._getTopOfText(self.location[1], canvasObject)
		graphicsObject.DrawText(canvasObject, self.font.font, x, y, self.font.color, self.text)
	
	def _getPositionForCenterText(self, canvasObject):
		stringLength = len(self.text)
		centerOfScreen = canvasObject.width/2
		textWidth = self.font.width * stringLength
		return centerOfScreen - textWidth/2
	
	def _getTopOfText(self, y, canvasObject):
		if self.displayFromBottom:
			return canvasObject.height - y
		else:
			return y + self.font.height -1