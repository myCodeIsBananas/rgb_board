import time, sys, os
sys.path.append(os.path.abspath(os.path.dirname(__file__) + '/..'))
from rgbmatrix import RGBMatrix, RGBMatrixOptions, graphics

class DisplayDriver():
	
	screenWidth 		= 64
	screenHeight 		= 32
	canvas 				= None
	debug				= False # Causes performance loss, but may be helpful for debugging.
	displayList			= []
	
	def __init__(self, *args, **kwargs):
		# Setup Options for matrix.
		matrixOptions = RGBMatrixOptions()

		#matrixOptions.hardware_mapping = 'regular'
		matrixOptions.rows 			= self.screenHeight
		matrixOptions.cols 			= self.screenWidth
		matrixOptions.chain_length 	= 1 # How many boards are chained together.
		matrixOptions.parallel 		= 1
		matrixOptions.row_address_type = 0
		matrixOptions.multiplexing 	= 0
		matrixOptions.pwm_bits 		= 11
		matrixOptions.brightness 	= 50 #0-100
		matrixOptions.pwm_lsb_nanoseconds 	= 130
		matrixOptions.led_rgb_sequence 		= "RGB"
		matrixOptions.pixel_mapper_config 	= ""
		matrixOptions.gpio_slowdown 		= 1

		self.matrix = RGBMatrix(options = matrixOptions)
		self._initDisplay()
		
	def addDisplayItem(self, displayItem):
		self.displayList.append(displayItem)
		
	def flush(self):
		self._clearDisplay()
		
		for displayItem in self.displayList:
			displayItem.display(graphics, self.canvas)
		
		self._flushDisplay()
	
	def _initDisplay(self):
		self.canvas = self.matrix.CreateFrameCanvas()
		
	def _clearDisplay(self):
		self.canvas.Clear()
		
	def _flushDisplay(self):
		time.sleep(0.05)
		self.canvas = self.matrix.SwapOnVSync(self.canvas)
		self.displayList = []
	