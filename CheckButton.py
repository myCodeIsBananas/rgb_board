import RPi.GPIO as GPIO

class CheckButton():

	gpioNumber = 0
	GPIO.setmode(GPIO.BCM)
	
	def __init__(self, _gpioNumber):
		GPIO.setup(_gpioNumber, GPIO.IN, pull_up_down=GPIO.PUD_UP)
		self.gpioNumber = _gpioNumber
		
	def isButtonPushed(self):
		input_state = GPIO.input(self.gpioNumber)
		if input_state == False:
			return True
		else:
			return False