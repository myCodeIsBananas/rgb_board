from rgbmatrix import graphics

class DisplayFont():
	
	width 	= 0
	height 	= 0
	font 	= None
	color 	= None
	
	def __init__(self, _fontPath, _fontWidth, _fontHeight, _fontColor):
	
		self.width = _fontWidth
		self.height = _fontHeight
		
		# Have to create new variable and assign it to class, going straight to graphics font overwrote font in other instances of this class.
		font  = graphics.Font()
		font.LoadFont(_fontPath)
		self.font = font
		
		self.color = _fontColor;